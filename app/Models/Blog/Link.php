<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
class Link extends Model
{
    use CrudTrait;

    protected $fillable =	['name', 'link'];
}
