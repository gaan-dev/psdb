<?php

namespace App\Models\Generic;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Models\Generic\Comment;
use App\Models\Generic\Image;
use App\Models\Generic\Video;

use App\Models\Generic\User;

class Report extends Model
{
    use CrudTrait;

    
	protected $fillable = [
		'body'
	];

    public function reportable()
    {
        return $this->morphTo();
    }

    public function link()
    {
        switch($this->reportable_type){
            case('App\Models\Generic\Comment'):
                return '<a class="btn btn-xs btn-default" href="'. Comment::find($this->reportable_id)->link() .'"><i class="fa fa-search"></i> View</a>';
                break;
            case('App\Models\Generic\Image'):
                return '<a class="btn btn-xs btn-default" href="'. Image::find($this->reportable_id)->link() .'"><i class="fa fa-search"></i> View</a>';
                break;
            case('App\Models\Generic\Video'):
                return '<a class="btn btn-xs btn-default" href="'. Video::find($this->reportable_id)->link() .'"><i class="fa fa-search"></i> View</a>';
                break;
        }
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
