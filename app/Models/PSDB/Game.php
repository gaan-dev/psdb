<?php

namespace App\Models\PSDB;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Laravel\Scout\Searchable;

use App\Models\PSDB\Group;
use App\Models\PSDB\Trophy;
use App\Models\PSDB\Platform;
use App\Models\Generic\Comment;
use App\Models\Generic\Image;
use App\Models\Generic\Video;

class Game extends Model
{
    use Searchable, CrudTrait;
    protected $fillable = [
        'npcommid',
        'title',
        'english_title',
        'description'
    ];
    
    public function groups()
    {
    	return $this->hasMany(Group::class);
    }

    public function trophies()
    {
    	return $this->hasManyThrough(Trophy::class, Group::class);
    }

    public function platforms()
    {
        return $this->belongsToMany(Platform::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function videos()
    {
        return $this->morphMany(Video::class, 'videoable');
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return $this->toArray(); 
    }
}
