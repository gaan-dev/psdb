<?php

namespace App\Models\PSDB;

use Illuminate\Database\Eloquent\Model;
use App\Models\PSDB\Game;

class Platform extends Model
{
	protected $fillable = ['title'];
	
    public function games()
    {
    	return $this->belongsToMany(Game::class);
    }
}
