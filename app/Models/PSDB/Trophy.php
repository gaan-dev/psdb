<?php

namespace App\Models\PSDB;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use App\Models\PSDB\Group;
use App\Models\PSDB\Type;
use App\Models\Generic\Comment;
use App\Models\Generic\Image;
use App\Models\Generic\Video;
use App\Models\Generic\User;

class Trophy extends Model
{
    use CrudTrait;
    
    protected $fillable = [
        'title',
        'description',
        'type_id',
        'hidden',
        'rarity',
        'group_id'
    ];
    
    
    public function group()
    {
    	return $this->belongsTo(Group::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function videos()
    {
        return $this->morphMany(Video::class, 'videoable');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'psdb.trophy_user');
    }
}
