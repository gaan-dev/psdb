<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Generic\User;

use Illuminate\Support\Facades\Log;

class UpdateAllUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('Running UpdateAllUsers Job');
        $users = User::where('psn_name', '!=', null)->get();
        foreach($users as $user){
            Log::debug('Adding new event:: UserRegisteredWithPSN :: ' . $user->psn_name);
            event(new \App\Events\UserRegisteredWithPSN($user));
            Log::debug('Added new event:: UserRegisteredWithPSN :: ' . $user->psn_name);
        }    
    }
}
