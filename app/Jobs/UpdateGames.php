<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\UpdateGame;
use App\Models\PSDB\Game;
use PSNAPI;

use Illuminate\Support\Facades\Log;


class UpdateGames implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $psn_api;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('Running UpdateGames Job');
        $this->psn_api = new PSNAPI();
        $update_list = $this->getUpdateList();
        foreach($update_list as $game_to_add){
            UpdateGame::dispatch($game_to_add['npcommid']);
        }
        Log::debug('Finished Running UpdateGames Job');
    }

    /**
        Gets a list of games currently in the database and all games returned by the API, checks through each item in the
        remote game list to see if they've already been added to the system and if not, adds them to an array of games to 
        be updated.

        Once it has checked all those games, returns that array of games to be updated
    */
    private function getUpdateList(){
        $local_games = Game::all()->pluck('npcommid');
        $remote_games = $this->psn_api->getListOfGames();
        $update_games = array();

        foreach($remote_games['game'] as $game){
            if(!$local_games->contains($game['npcommid'])){
                $update_games[] = $game;
            }
        }
        return $update_games;
    }
}
