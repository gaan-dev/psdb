<?php

namespace App\Listeners;

use App\Events\UserRegisteredWithPSN;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

use App\Models\PSDB\Game;
use App\Models\PSDB\Trophy;
use PSNAPI;

class GetUserPSNData implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegisteredWithPSN  $event
     * @return void
     */
    public function handle(UserRegisteredWithPSN $event)
    {
        Log::debug('Updating User Games and Trophies :: ' . $event->user->id);
        $psn_api = new PSNAPI();
        $api_games = $psn_api->getCompleteUserData($event->user->psn_name);
        foreach($api_games as $api_game){
            $game = Game::where('npcommid', $api_game['npcommid'])->with('trophies')->first();
            if($game){
                if(!$event->user->games->contains($game)){
                    $event->user->games()->save($game);
                }
                //Get the first trophy for the game
                //Then, offset by id returned by api minus one. ie;
                // Trophy ID from local game would be 94, trophy ID from api would be 4. 94+4-1
                $offset = $game->trophies()->first()->id - 1;           
                foreach($api_game['trophies'] as $api_trophy){  
                    $trophy = Trophy::find($offset + $api_trophy['id']);
                    if(!$event->user->trophies->contains($trophy)){
                        $event->user->trophies()->save(Trophy::find($offset + $api_trophy['id']));
                    }                  
                }
            }
        }
        Log::debug('Finished Updating User Games and Trophies :: ' . $event->user->id);
    }
}
