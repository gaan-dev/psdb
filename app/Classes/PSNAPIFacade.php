<?php

namespace App\Classes;

use Illuminate\Support\Facades\Facade;

class PSNAPIFacade extends Facade{
    protected static function getFacadeAccessor() { return 'psnapi'; }
}