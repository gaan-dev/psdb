<?php

namespace App\Classes;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Parser;

class PSNAPI{

	private $client;
	private $parser;

	//https://www.happynation.co.uk/api

	//psnGetUser - user_id
	//psnGetUserGames - user_id
	//psnGetUserTrophies - npcommid, user_id
	//psnGetUserUpdates - user_id
	//psnGetTrophies - npcommid
	//psnImportUser - user_id
	//psnAuthUser - user_id 
	//psnGetGame - npcommid
	//psnListGames
	//psnPopularThisWeek
	function __construct() {		
		$this->client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => 'https://www.happynation.co.uk/api/',
		]);
		$this->parser = new Parser();
	}

	public function popularThisWeek()
	{
		$response = $this->makeRequest('psnPopularThisWeek')->getBody()->getContents();
		return $this->parser::xml($response);
	}

	public function getUser($username){
		$response = $this->makeRequest('psnGetUser', ['user_id' => $username])->getBody()->getContents();
		return $this->parser::xml($response);
	}

	public function importUser($username)
	{
		$response = $this->makeRequest('psnImportUser', ['user_id' => $username])->getBody()->getContents();
		return $this->parser::xml($response);
	}

	public function authUser()
	{
		$response = $this->makeRequest('psnAuthUser', ['user_id' => $username])->getBody()->getContents();
		return $this->parser::xml($response);
	}

	public function getUserGames($username){
		$response = $this->makeRequest('psnGetUserGames', ['user_id' => $username])->getBody()->getContents();
		return $this->parser::xml($response);
	}

	public function getUserTrophies($npcommid, $username)
	{
		$response = $this->makeRequest('psnGetUserTrophies', ['user_id' => $username, 'npcommid' => $npcommid])->getBody()->getContents();
		return $this->parser::xml($response);
	}

	public function getListOfGames($platform = 'all'){
		$response = $this->makeRequest('psnListGames', ['platform' => $platform])->getBody()->getContents();
		return $this->parser::xml($response);
	}

	public function getGame($npcommid){
		$response = $this->makeRequest('psnGetGame', ['npcommid' => $npcommid])->getBody()->getContents();
		return $this->parser::xml($response);
	}

	public function getTrophies($npcommid){
		$response = $this->makeRequest('psnGetTrophies', ['npcommid' => $npcommid])->getBody()->getContents();
		return $this->parser::xml($response);
	}

	private function makeRequest($path, $params=[]){
		$p = array('form_params' => array_merge(
			[
				'api_key'	=>	config('app.psn_key'),
				'api_secret'	=>	config('app.psn_secret')
			],
			$params
		));
		return $this->client->request('POST', $path, $p);
	}

	//Move this to PSNAPI as a consolidated method for getting the full information needed.
    //Refactor asap, this is gross
    /**
        Grabs the complete set of data needed to complete a game insertion
    */
    public function getCompleteDataSet($npcommid){
        $api_game = $this->getGame($npcommid);
        $api_trophy_data = $this->getTrophies($npcommid);
        $image_prefix = $api_trophy_data['image_prefix'];
        if(isset($api_trophy_data['groups'])){
        	$api_groups = $api_trophy_data['groups'];
        }else{
        	$api_groups = [];
        }
        $api_trophies = $api_trophy_data['trophies'];

        $base_game_group = [
            'id'    =>  0,
            'title' =>  $api_game['title'],
            'description'   =>  null,
            'image' =>  $api_game['image']
        ];

        if(empty($api_groups)){
            $api_groups[] = $base_game_group;
        }else{ 
            //Add the image_prefix to the image suffix for each group
            foreach($api_groups as $key=>$group){ //Don't know if Keying is necessary, but better to be safe than sorry.
            	if($group == "Empty Node"){
            		unset($api_groups[$key]);
            		continue;
            	}
                $api_groups[$key]['image'] = $image_prefix . $group['image'];
            }
            //Stick the base game group as the first item in our array of groups
            array_unshift($api_groups, $base_game_group);
        }

        //In the original versions, we had the groups being associated at this point. Wont work in this version
        // Instead, we'll have to give add trophies the group_id at the time its being inserted?
        if(!empty($api_trophies)){
            foreach($api_trophies as $key=>$trophy){
                $api_trophies[$key]['image'] = $image_prefix . $trophy['image'];
            }
        }

        return [
            'game'  =>  $api_game,
            'groups'    =>  $api_groups,
            'trophies'  =>  $api_trophies,
        ];
    }

    public function getCompleteUserData($psn_name)
    {
    	$user_games = $this->getUserGames($psn_name);
    	foreach($user_games['games'] as $game_key=>$game){
    		if($game == "Empty Node"){
            	unset($user_games['games'][$game_key]);
            	continue;
    		}
    		$trophy_data = $this->getUserTrophies($game['npcommid'], $psn_name);
    		if($trophy_data['success']){
    			$user_games['games'][$game_key]['trophies'] = $trophy_data['trophies'];
    			foreach($user_games['games'][$game_key]['trophies'] as $trophy_key=>$trophy){
	    			if($trophy == "Empty Node"){
	    				unset($user_games['games'][$game_key]['trophies'][$trophy_key]);
	    			}
	    		}
    		}
    		else{
    			$user_games['games'][$game_key]['trophies'] = [];
    		}
    		// $user_games['games'][$game_key]['trophies'] = 
    		
    	}
    	return $user_games['games'];
    }

}