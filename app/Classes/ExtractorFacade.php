<?php

namespace App\Classes;

use Illuminate\Support\Facades\Facade;

class ExtractorFacade extends Facade{
    protected static function getFacadeAccessor() { return 'extractor'; }
}