<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PSDB\Game;


class SearchController extends Controller
{
    public function search(Request $request)
    {
    	return Game::search($request->search)->get()->values();
    }
}
