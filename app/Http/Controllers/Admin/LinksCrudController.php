<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;


class LinksCrudController extends CrudController
{
    public function setup()
    {
    	$this->crud->setModel("App\Models\Blog\Link");
    	$this->crud->setRoute("admin/links");
    	$this->crud->setEntityNameStrings('link', 'links');

    	$this->crud->setColumns(['name', 'link']);
    	$this->crud->addField([
    		'name'	=>	'name',
  			'label'	=>	'Link Name'
    	]);
    	$this->crud->addField([
			'name'	=>	'link',
			'label'	=>	'Link',
		]);
    }

    public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
