<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;


class TagsCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel("App\Models\Blog\Tag");
        $this->crud->setRoute("admin/tags");
        $this->crud->setEntityNameStrings('tag', 'tags');

        $this->crud->setColumns(['name']);
        $this->crud->addField([
            'name'  =>  'name',
            'label' =>  'Link Name'
        ]);
        $this->crud->addField([ 
            'name' => 'type',
            'label' => "Color",
            'type' => 'select_from_array',
            'options' => [
                'black'   =>  'Black',
                'dark' => 'Dark',
                'light'   =>  'Light',
                'white'  =>  'White',
                'primary'   => 'Primary',
                'info'    =>  'Info',
                'success'    =>  'Success',
                'warning'    =>  'Warning',
                'danger'    =>  'Danger',
                ],
            'allows_null' => false,
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
