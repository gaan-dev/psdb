<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;


class ReportsCrudController extends CrudController
{
    public function setup()
    {
    	$this->crud->setModel("App\Models\Generic\Report");
    	$this->crud->setRoute("admin/reports");
    	$this->crud->setEntityNameStrings('report', 'reports');

    	$this->crud->setColumns(
        [
            [
                'label' =>  'Reports',
                'type'  =>  'select',
                'name'  =>  'reportable_type',
                'entity'    =>  'reportable',
                'attribute' =>  'body'
            ]
        ]);
        $this->crud->addButtonFromModelFunction('line', 'View', 'link', 'beginning');
    }

    public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
