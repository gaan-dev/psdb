<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;


class GroupsCrudController extends CrudController
{
    public function setup()
    {
    	$this->crud->setModel("App\Models\PSDB\Group");
    	$this->crud->setRoute("admin/groups");
    	$this->crud->setEntityNameStrings('group', 'groups');
        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');

    	$this->crud->setColumns(
        [
            [
                'name'  => 'title',
                'label' => 'Title',
                'type'  => 'text',
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Game',
                'type'      => 'select',
                'name'      => 'game', // the method that defines the relationship in your Model
                'entity'    => 'game', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => "App\Models\PSDB\Game", // foreign key model
                'pivot'     => false, // on create&update, do you need to add/delete pivot table entries?
            ],
        ]);
    	$this->crud->addFields([
            [
        		'name'	=>	'title',
      			'label'	=>	'Title',
                'tab'   =>  'Group'
            ],
            [
                'name'  =>  'description',
                'type'  =>  'ckeditor',
                'label' =>  'Description',
                'tab'   =>  'Group'
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Game',
                'type'      => 'select2',
                'name'      => 'game_id', // the method that defines the relationship in your Model
                'entity'    => 'game', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => "App\Models\PSDB\Game", // foreign key model
                'pivot'     => false, // on create&update, do you need to add/delete pivot table entries?
                'tab'   =>  'Group'
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Trophy',
                'type'      => 'select2_multiple',
                'name'      => 'trophies', // the method that defines the relationship in your Model
                'entity'    => 'trophies', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => "App\Models\PSDB\Trophy", // foreign key model
                'pivot'     => false, // on create&update, do you need to add/delete pivot table entries?
                'tab'   =>  'Group'
            ],
    	]);
    	
    }

    public function store(StoreRequest $request)
	{
        return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
