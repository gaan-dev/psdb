<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;


class PostsCrudController extends CrudController
{
    public function setup()
    {
    	$this->crud->setModel("App\Models\Blog\Post");
    	$this->crud->setRoute("admin/posts");
    	$this->crud->setEntityNameStrings('post', 'posts');

    	$this->crud->setColumns(
        [
            [
                'name'  => 'title',
                'label' => 'Title',
                'type'  => 'text',
            ],
            [
                // n-n relationship (with pivot table)
                'label'     => 'Tags',
                'type'      => 'select_multiple',
                'name'      => 'tags', // the method that defines the relationship in your Model
                'entity'    => 'tags', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Blog\Tag", // foreign key model
                'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            ],
        ]);
    	$this->crud->addField([
    		'name'	=>	'title',
  			'label'	=>	'Post Title'
    	]);
    	$this->crud->addField([
			'name'	=>	'body',
			'label'	=>	'Post Body',
            'type'  =>  'ckeditor'
		]);
        $this->crud->addField([
            'label'     => 'Tags',
            'type'      => 'checklist',
            'name'      => 'tags',
            'entity'    => 'tags',
            'attribute' => 'name',
            'model'     => "App\Models\Blog\Tag",
            'pivot'     => true,
        ]);
    }

    public function store(StoreRequest $request)
	{
		$this->crud->hasAccessOrFail('create');

        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }

        // replace empty values with NULL, so that it will work with MySQL strict mode on
        foreach ($request->input() as $key => $value) {
            if (empty($value) && $value !== '0') {
                $request->request->set($key, null);
            }
        }

        $request->request->set('user_id', Auth::id());

        // insert item in the db
        $item = $this->crud->create($request->except(['save_action', '_token', '_method']));
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->setSaveAction();

        return $this->performSaveAction($item->getKey());
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
