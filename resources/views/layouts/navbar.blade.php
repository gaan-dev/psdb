<div>
<div class="hero-body is-hidden-touch" style="margin-bottom: -55px; padding-right: 0px;">
    <a href="/"><img src="{{ asset('/storage/psdb_logo.png')}}" alt="PSDB" width="201" height="79"></a>
    <div class="is-pulled-right">
        <div class="field has-addons">
            <div class="control">
                <type-ahead>
                
                </type-ahead>
            </div>
            <div class="control">
                <button class="button">Search</button>
            </div>
        </div>        
    </div>
</div>
<div>
    <nav class="navbar">
        <div class="navbar-brand" style="overflow:visible;">
            <div class="navbar-item is-hidden-desktop">
                <div class="field has-addons">
                    <div class="control is-expanded">
                        <type-ahead fullwidth="true">
                        
                        </type-ahead>
                    </div>
                    <div class="control">
                        <button class="button">Search</button>
                    </div>
                </div> 
            </div>
            <div class="navbar-burger burger" data-target="navMain">
              <span></span>
              <span></span>
              <span></span>
            </div>

        </div>
        <div class="navbar-menu" id="navMain">
            <div class="navbar-start">
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="/database">
                        Database
                    </a>
                    <div class="navbar-dropdown ">
                        <a class="navbar-item " href="/games">
                            Games
                        </a>
                        <a class="navbar-item " href="/groups">
                            Groups
                        </a>
                        <a class="navbar-item " href="/trophies">
                            Trophies
                        </a>
                    </div>
                </div>
            </div>

            <div class="navbar-end">
                @if(Auth::guest())
                    <a class="navbar-item" href="/login">Login</a>
                    <a class="navbar-item" href="/register">Register</a>
                @else
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link" href="/profile">
                          {{ Auth::user()->name }}
                        </a>
                        <div class="navbar-dropdown" style="z-index: 30">
                            @role('Administrator')
                            <a href="/admin" class="navbar-item">Administration</a>
                            @endrole
                            <a href="/profile/settings" class="navbar-item">Settings</a>
                            <a href="{{ route('logout') }}" class="navbar-item" 
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                        </div>    
                    </div>
                @endif
            </div>
        </div>
    </nav>
</div>

<nav class="level ad-bar">    
    <div class="level-item" style="height: 100px; width: 500px">
        
    </div>
</nav>


<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
</div>

@section('postscripts')
<script>
document.addEventListener('DOMContentLoaded', function () {

  // Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any nav burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(function ($el) {
      $el.addEventListener('click', function () {

        // Get the target from the "data-target" attribute
        var target = $el.dataset.target;
        var $target = document.getElementById(target);

        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
        $el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }

});
</script>
@endsection