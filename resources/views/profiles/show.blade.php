@extends('layouts.master')

@section('content')
<section>
	<div class="card">
		<header class="card-header">
			<p class="card-header-title">
				{{ $user->name }}s profile
			</p>
		</header>
		<tabs>
			@if($user->publish_comments)
			<tab name="My Posts" selected="true">
				<div class="card-content">
				@if(count($user->comments))
					@foreach($user->comments as $comment)
						@include('comments.show')
					@endforeach
				@else
					We couldn't find any comments for this user!
				@endif
				</div>
			</tab>
			@endif
			@if($user->publish_images)
			<tab name="My Images">
				<div class="card-content">
					@if(count($user->images))
						<div class="columns is-multiline">
							@foreach($user->images as $image)
								<div class="column is-3">
									<figure class="image is-square profile-image" style="background-image: url('{{ Storage::disk('s3')->url($image->src) }}');">
									</figure>
									@if(count($image->trophies))
									Posted to 
									<a href="/trophies/{{ $image->trophies->first()->id }}">
										{{ $image->trophies->first()->title }}
									</a>
									 {{ $image->trophies->first()->created_at->diffForHumans() }}									
									@elseif(count($image->groups))
									Posted to 
									<a href="/groups/{{ $image->groups->first()->id }}">
										{{ $image->groups->first()->title }}
									</a>
									 {{ $image->groups->first()->created_at->diffForHumans() }}
									@elseif(count($image->games))
									Posted to 
									<a href="/games/{{ $image->games->first()->id }}">
										{{ $image->games->first()->title }} 
									</a>
									 {{ $image->games->first()->created_at->diffForHumans() }}
									@endif
								</div>
							@endforeach		
						</div>
					@else
						We couldn't find any images from this user!
					@endif
				</div>
			</tab>
			@endif
			@if($user->publish_games)
			<tab name="My Games">
				<div class="card-content">
					@if(count($user->games))
						<div class="columns is-multiline">
							@foreach($user->games as $game)
								<div class="column is-3">
									<div class="notification" style="padding: 1em">
										<figure class="image is-2by1 profile-image" style="background-image: url('{{ Storage::disk('s3')->url('images/' . $game->id . '/game_logo.PNG') }}');">
										</figure>
										<a href="/games/{{ $game->id }}"><h6 class="title is-5 has-text-centered">{{ $game->title }}</h6></a><br/>
										<progress class="progress is-info" value="{{ $user->progress($game) }}" max="100">{{ $user->progress($game) }}%</progress>
										<p class="has-text-centered">{{ $user->trophyCount($game) }}/{{ count($game->trophies) }} ({{ $user->progress($game) }}%)</p>
									</div>
								</div>
							@endforeach		
						</div>
					@else
						We haven't managed to collect any data for your account yet.
					@endif					
				</div>
			</tab>
			@endif
		</tabs>
		@if(!$user->publish_comments && !$user->publish_images && !$user->publish_games)
		<div class="card-content">
			This user has chosen to not show anything yet
		</div>
		@endif
	</div>
</section>
@endsection