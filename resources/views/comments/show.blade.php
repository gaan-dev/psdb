<div class="comment-wrapper">
	<div class="is-pulled-right">
		@if($comment->user_id != Auth::id() && !Auth::guest() && !count($comment->reports))
			<report src="/comments/{{ $comment->id }}/report"></report>
		@endif
		@if($comment->user_id == Auth::id())
			<a class="delete"></a>
		@endif
	</div>
	<small><span style="display: block">Posted {{ $comment->created_at->diffForHumans() }} by <a href="/profile/{{ $comment->user->id }}">{{ $comment->user->name }}</a></span></small>
	<span style="display: block">{!! $comment->body !!}</span>
</div>
<hr>