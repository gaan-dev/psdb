@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="columns">
            <div class="column is-three-quarters">
                <div class="card module">
                    <div class="card-content">
                        <div class="level is-header">
                            <div class="level-left">
                                <div class="level-item">
                                    <a class="title is-4" href="/posts/{{ $post->id }}">
                                        {{ $post->title }}
                                    </a>
                                </div>
                                <div class="level-item">
                                    @foreach($post->tags as $tag)
                                        <a class="tag-link" href="?tag={{ $tag->id }}">
                                            <span class="tag is-{{ $tag->type }}">
                                                {{ $tag->name }}
                                            </span>
                                        </a>
                                    @endforeach
                                </div>
                            </div>                        
                        </div>
                        <p class="blog-post-meta">
                            {{ $post->created_at->formatLocalized('%A %d %B %Y') }} 
                            by 
                            <a href="#">
                                {{ $post->owner->name }}
                            </a>
                        </p>
                        <div class="content">
                            {!! $post->body !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                @include('layouts.sidebar')
            </div>
        </div>
    </div>  
@endsection
