@extends('layouts.master')

@section('content')
<div class="card">
    <header class="card-header">
        <p class="card-header-title">
            Reset Password
        </p>
    </header>
    <div class="card-content">
        <div class="columns">
            <div class="column is-6 is-offset-3">
                @if(session('status'))
                    <div class="notification is-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{ route('password.email') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="field">
                        <label for="email" class="label">E-mail Address</label>
                        <div class="control">
                            <input id="email" type="email" class="input @if($errors->has('name')) is-danger @endif" name="email" value="{{ old('email') }}" required>
                        </div>
                        @if ($errors->has('email'))
                            <p class="help is-danger">
                                <small>{{ $errors->first('email') }}</small>
                            </p>
                        @endif
                    </div>
                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary">Send Password Reset Link</button>
                        </div>
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
@endsection
