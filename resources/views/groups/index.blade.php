@extends('layouts.master')

@section('content')
	<section>
		<div class="card">
			<table class="table is-fullwidth">
				<thead>
					<tr>
						<td>Groups</td>
					</tr>
				</thead>
				<tbody>
					@foreach($groups as $group)
					<tr>
						<td><a href="/groups/{{ $group->id }}">{{ $group->title }}</a></td>
					</tr>
					@endforeach
				</tbody>
				<thead>
					<tr>
						<td>Group</td>
					</tr>
				</thead>
			</table>
			<div class="card-content">
				{{ $groups->links() }}	
			</div>
		</div>
	</section>
@endsection