{{ csrf_field() }}
Select image to upload:
<input type="file" name="image" class="button" id="image-upload" @if(Auth::guest()) disabled @endif>
<input type="submit" class="button" value="Upload Image" name="submit" @if(Auth::guest()) disabled @endif>
@include('layouts.errors')