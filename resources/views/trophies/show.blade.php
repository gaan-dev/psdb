@extends('layouts.master')

@section('content')
	<section>
		<div class="card">
			<header class="card-header">
				<nav class="breadcrumb has-bullet-separator" aria-label="breadcrumbs">
				  <ul>
				    <li><a href="#">Database</a></li>
				    <li><a href="/games/{{ $trophy->group->game->id }}">{{ $trophy->group->game->title }}</a></li>
				    <li><a href="/groups/{{ $trophy->group->id }}">{{ $trophy->group->title }}</a></li>
				    <li><a href="/trophies/{{ $trophy->id }}">{{ $trophy->title }}</a></li>
				  </ul>
				</nav>
			</header>
			<div class="card-content">
				<div class="columns">
					<div class="column">
						<figure class="image circle-image" 
						style=" background-image: url({{ Storage::disk('s3')->url('images/' . $trophy->group->game->id . '/' . $trophy->group->id . '/' . $trophy->id . '/trophy_logo.PNG') }});
						">
						</figure>
						<h1 class="title">{{ $trophy->title }}</h1>
						{{ $trophy->description }}
					</div>
					<div class="column is-3">
						<div class="box light-box">
							<h1 class="title is-6 has-text-centered">
								Groups
							</h1>
							<ul>
							@foreach($trophy->group->game->groups as $group)
								<li>
									<a href="/groups/{{ $group->id }}">
										@if($trophy->group->game->title == $group->title)
											Base Game
										@else
											{{ $group->title }}
										@endif
									</a>
								</li>
							@endforeach
							</ul>
						</div>
					</div>
					<div class="column is-4">
						<div class="box light-box">
							<h1 class="title is-4 has-text-centered">
								Quick Facts
							</h1>
							
							<h1 class="title is-6 has-text-centered" style="margin-bottom: 0.3em; margin-top: 0.7em">
								Images
							</h1>
							@if(count($trophy->images))
							<image-gallery>
								@foreach($trophy->images as $image)
								<image-item path="{{ Storage::disk('s3')->url($image->src) }}"
								@if($trophy->images->first()->id == $image->id)
								:active=true
								@endif >
								</image-item>
								@endforeach
							</image-gallery>
							@else
								<div class="has-text-centered">
									There doesn't appear to be any images submitted yet! <a href="#image">Be the first!</a>
								</div>
							@endif							
							<h1 class="title is-6 has-text-centered" style="margin-bottom: 0.3em; margin-top: 0.7em">
								Videos
							</h1>
							
							@if(count($trophy->videos))
							<video-gallery>
								@foreach($trophy->videos as $image)
								<video-item id="{{ $video->id }}"
								@if($trophy->images->first()->id == $image->id)
								:active=true
								@endif>
								</video-item>
								@endforeach
							</video-gallery>
							@else
								<div class="has-text-centered">
									There doesn't appear to be any videos submitted yet! <a href="#videos">Be the first!</a>
								</div>
							@endif
						</div>
					</div>
					
				</div>
			</div>
			<tabs>
				<tab name="Trophies" selected="true">
					<table class="table is-fullwidth is-narrow">
						<thead>
							<tr>
								<td>Type</td>
								<td>Trophy</td>
								<td>Rarity</td>
								<td>Hidden</td>
							</tr>
						</thead>
						<tbody>
							@foreach($trophy->group->trophies as $a_trophy)
							<tr class="trophy-row 
								@if($a_trophy->id === $trophy->id) 
								is-selected 
								@endif
								@if(Auth::check())
									@if(Auth::user()->trophies->contains($a_trophy->id)) 
									 has-trophy 
									@endif
								@endif">
								<td>
									<div class="psdb-icon 
									@if($a_trophy->type->id == 1)
										plat-icon
									@elseif($a_trophy->type->id == 2)
										gold-icon
									@elseif($a_trophy->type->id == 3)
										silver-icon
									@elseif($a_trophy->type->id == 4)
										bronze-icon
									@endif
									">										
									</div>
								</td>
								<td>
									<div class="psdb-icon" 
										style="
										margin-right:0.3em;
										float:left;
										background-size: 100% 100%; 
										background-image: url({{ Storage::disk('s3')->url('images/' . $a_trophy->group->game->id . '/' . $a_trophy->group->id . '/' . $a_trophy->id . '/trophy_logo.PNG') }});">										
									</div>
									<span style="display:block">
										<a href="/trophies/{{ $a_trophy->id }}">{{ $a_trophy->title }}</a>
									</span>
									<small style="display:block">
										{{ $a_trophy->description }}
									</small>
								</td>
								<td>
									{{ $a_trophy->rarity }}%
								</td>
								<td>
									@if($a_trophy->hidden)
										Yes
									@else
										No
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</tab>
				<tab name="Comments">
					@if(count($trophy->comments))
						@foreach($trophy->comments as $comment)
							@include('comments.show')
						@endforeach
					@else
					<div class="card-content has-text-centered">
						There doesn't appear to be anything here!<br/>
						@if(Auth::guest())
							<a href="/login">Sign in</a> or <a href="/register">register</a> to leave a comment.
						@else
							<a href="#">Leave a comment</a>
						@endif
					</div>
					@endif
				</tab>
			</tabs>			
		</div>
	</section>
	<section>
		<div class="card">
			<header class="card-header">
				<p class="card-header-title">
					Contribute
				</p>
			</header>
			<tabs>
				<tab name="Comment" selected="true">
					<div class="card-content">
						<form action="/trophies/{{ $trophy->id }}/comment" method="POST">
						@include('forms.comment')
						</form>
					</div>
				</tab>
				<tab name="Submit your Screenshot">
					<div class="card-content">
						<form action="/trophies/{{ $trophy->id }}/image" type="file" method="post" enctype="multipart/form-data">
						@include('forms.image')
						</form>						
					</div>
				</tab>
				<tab name="Suggest a Video">
					<div class="card-content">
						<form action="/trophies/{{ $trophy->id }}/video" method="POST">
						@include('forms.video')
						</form>
					</div>
				</tab>
			</tabs>
		</div>
	</section>			
@endsection