@extends('layouts.master')

@section('content')
	<section>
		<div class="card">
			<table class="table is-fullwidth">
				<thead>
					<tr>
						<td>Trophies</td>
					</tr>
				</thead>
				<tbody>
					@foreach($trophies as $trophy)
					<tr>
						<td><a href="/trophies/{{ $trophy->id }}">{{ $trophy->title }}</a></td>
					</tr>
					@endforeach
				</tbody>
				<thead>
					<tr>
						<td>Trophy</td>
					</tr>
				</thead>
			</table>
			<div class="card-content">
				{{ $trophies->links() }}	
			</div>
		</div>
	</section>
@endsection