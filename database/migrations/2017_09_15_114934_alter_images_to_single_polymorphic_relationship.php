<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterImagesToSinglePolymorphicRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function (Blueprint $table) {
             $table->string('imageable_type');
             $table->integer('imageable_id');
        });

        $imageables = DB::select('select * from imageables');

        foreach($imageables as $image_link){
            DB::update('update images set imageable_id = ?, imageable_type = ? where id = ?', [$image_link->imageable_id, $image_link->imageable_type, $image_link->image_id]);
        }

        Schema::dropIfExists('imageables');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('imageables', function(Blueprint $table){
            $table->integer('image_id');
            $table->integer('imageable_id');
            $table->string('imagesable_type');
        });

        Schema::table('images', function (Blueprint $table) {
            $table->dropColumn(['imageable_type', 'imageable_id']);
        });
    }
}
