<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class AlterCommentsToSinglePolymorphicRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
             $table->string('commentable_type');
             $table->integer('commentable_id');
        });

        $commentables = DB::select('select * from commentables');

        foreach($commentables as $comment_link){
            DB::update('update comments set commentable_id = ?, commentable_type = ? where id = ?', [$comment_link->commentable_id, $comment_link->commentable_type, $comment_link->comment_id]);
        }

        Schema::dropIfExists('commentables');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('commentables', function(Blueprint $table){
            $table->integer('comment_id');
            $table->integer('commentable_id');
            $table->string('commentable_type');
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->dropColumn(['commentable_type', 'commentable_id']);
        });
    }
}
