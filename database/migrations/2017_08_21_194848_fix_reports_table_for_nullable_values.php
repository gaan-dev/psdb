<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixReportsTableForNullableValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports', function(Blueprint $table){
            $table->integer('user_id')->unsigned()->nullable()->default(null)->change();
            $table->integer('reportable_id')->unsigned()->nullable()->default(null)->change();
            $table->string('reportable_type')->unsigned()->nullable()->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function(Blueprint $table){
            $table->integer('user_id')->change();
            $table->integer('reportable_id')->change();
            $table->string('reportable_type')->change();
        });
    }
}
