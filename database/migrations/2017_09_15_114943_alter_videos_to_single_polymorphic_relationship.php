<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVideosToSinglePolymorphicRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function (Blueprint $table) {
             $table->string('videoable_type');
             $table->integer('videoable_id');
        });

        $videoables = DB::select('select * from videoables');

        foreach($videoables as $video_link){
            DB::update('update videos set videoable_id = ?, videoable_type = ? where id = ?', [$video_link->videoable_id, $video_link->vidoeable_type, $video_link->video_id]);
        }

        Schema::dropIfExists('videoables');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('videoables', function(Blueprint $table){
            $table->integer('video_id');
            $table->integer('videoable_id');
            $table->string('videoable_type');
        });

        Schema::table('videos', function (Blueprint $table) {
            $table->dropColumn(['videoable_type', 'videoable_id']);
        });
    }
}
