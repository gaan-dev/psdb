<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMorphablesToAllowNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->string('commentable_type')->nullable()->default(null)->change();
            $table->integer('commentable_id')->nullable()->default(null)->change();
        });
        Schema::table('images', function (Blueprint $table) {
            $table->string('imageable_type')->nullable()->default(null)->change();
            $table->integer('imageable_id')->nullable()->default(null)->change();
        });
        Schema::table('videos', function (Blueprint $table) {
            $table->string('videoable_type')->nullable()->default(null)->change();
            $table->integer('videoable_id')->nullable()->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->string('commentable_type')->change();
            $table->integer('commentable_id')->change();
        });
        Schema::table('images', function (Blueprint $table) {
            $table->string('imageable_type')->change();
            $table->integer('imageable_id')->change();
        });
        Schema::table('videos', function (Blueprint $table) {
            $table->string('videoable_type')->change();
            $table->integer('videoable_id')->change();
        });
    }
}
