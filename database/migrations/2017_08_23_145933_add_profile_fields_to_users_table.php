<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfileFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){
            $table->integer('publish_comments')->nullable()->default(1);
            $table->integer('publish_images')->nullable()->default(1);
            $table->integer('publish_games')->nullable()->default(1)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->dropColumn('publish_comments');
            $table->dropColumn('publish_images');
            $table->string('publish_games')->nullable()->default(null)->change();
        });
    }
}
