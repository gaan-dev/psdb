<?php

use Illuminate\Database\Seeder;
use App\Models\PSDB\Type;
use App\Models\PSDB\Platform;
use App\Models\Generic\User;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
    	Type::create([
    		'title'	=>	'Platinum'
    	]);

    	Type::create([
    		'title'	=>	'Gold'
    	]);

    	Type::create([
    		'title'	=>	'Silver'
    	]);

    	Type::create([
    		'title'	=>	'Bronze'
    	]);

        Platform::create([
            'title' =>  'Playstation 3'
        ]);

        Platform::create([
            'title' =>  'Playstation 4'
        ]);

        Platform::create([
            'title' =>  'Playstation Vita'
        ]);

        Role::create([
            'name'  =>  'Administrator'
        ]);

        Role::create([
            'name'  =>  'Moderator'
        ]);

        User::create([
            'name'  =>  'Goms',
            'email' =>  'scgoms@gmail.com',
            'password'  =>  bcrypt('password')
        ]);
    }
}
